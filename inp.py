# Copyright (C) 2020 Benjamin Zwick
#
# Inp.Py: Read INP files in Python
#
# Inp.Py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Inp.Py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Inp.Py.  If not, see <https://www.gnu.org/licenses/>.

"""
Inp.Py is a Python module for reading INP files
commonly used with numerical simulation software
such as Abaqus and CalculiX.

Inp.Py (like the Python language itself)
is designed to be easy to read and extend.
The core functionality depends only on
the Python standard library.

Examples
--------

    import inp
    mesh = inp.read_inp("mymesh.inp")

See also at the end of this file for examples showing
how to convert the mesh to other file formats.

Notes
-----
- Only mesh entities (i.e. nodes, elements and sets) are imported,
  everything else is silently ignored.
- Surfaces are not yet supported.
"""

from enum import Enum
from collections import OrderedDict
import math


class Action(Enum):
    GET_NODE_IDS_AND_COORDINATES = 1
    GET_ELEM_IDS_AND_CONNECTIVITY = 2
    GET_SET_IDS = 3


def read_inp(fname):
    with open(fname) as f:
        lines = f.readlines()

    # Nodes
    node_ids = []               # node numbers
    node_coords = []            # node coordinates
    node_sets = OrderedDict()   # nsets

    # Elements
    elem_ids = []               # element numbers
    elem_types = []             # element types
    elem_conns = []             # element nodal connectivities
    elem_sets = OrderedDict()   # elsets

    node_set_name = False
    elem_set_name = False

    data_is_complete = True

    action = None

    try:
        line_number = 0
        for line in lines:
            line_number += 1

            line_is_comment = False
            line_is_keyword = False

            # Remove line endings and whitespace
            line = line.strip()

            # Skip comments...
            if line[0:2] == "**":
                line_is_comment = True
            # ...otherwise check if we need to get a new keyword from this line
            elif line[0] == "*":
                line_is_keyword = True
                # New keyword completes data even if previous line ended with a comma
                data_is_complete = True

            # Check if this is the last line
            if line_number == len(lines):
                # If EOF (end of file) we assume IDs are complete
                data_is_complete = True

            # Get more data before other actions
            if action in [Action.GET_ELEM_IDS_AND_CONNECTIVITY,
                          Action.GET_SET_IDS]:
                if not line_is_keyword and not line_is_comment:
                    _data, data_is_complete = _parse_data(line)
                    data += _data

            # Nodes
            if action == Action.GET_NODE_IDS_AND_COORDINATES:
                if not line_is_keyword and not line_is_comment:
                    i, xyz = _parse_node_id_and_coord(line)
                    node_ids += [i]
                    node_coords += [xyz]
                    if node_set_name:
                        node_sets[node_set_name] += [i]
            # Elements
            elif action == Action.GET_ELEM_IDS_AND_CONNECTIVITY:
                if data_is_complete and len(data) > 0:
                    # First ID is the element number
                    elem_id = int(data[0])
                    elem_ids += [elem_id]
                    elem_sets[elem_set_name] += [elem_id]
                    # Remaining IDs defines the nodal connectivity
                    elem_conns += [[int(i) for i in data[1::]]]
                    elem_types += [elem_type]
                    # Reset IDs
                    data = []
                    data_is_complete = False
            # Sets
            elif action == Action.GET_SET_IDS:
                # Ensure either node or element set but not both
                assert not (node_set_name and elem_set_name)
                assert node_set_name or elem_set_name
                if data_is_complete and len(data) > 0:
                    if node_set_name:
                        node_sets[node_set_name] += [int(i) for i in data]
                    elif elem_set_name:
                        elem_sets[elem_set_name] += [int(i) for i in data]
                    # Reset IDs
                    data = []
                    data_is_complete = False

            # Keyword
            if line_is_keyword:
                # Reset variables
                data = []
                data_is_complete = False
                elem_type = False
                node_set_name = False
                elem_set_name = False

                # First string is the keyword
                keyword = line.split(",")[0].strip()

                # Remaining strings are `parameter=value` pairs separated by commas
                key_val_pairs = line.split(",")[1::]
                dic = {}
                for key_val in key_val_pairs:
                    key, val = key_val.strip().split("=")
                    KEY = key.strip().upper()
                    val = val.strip()  # keep case because e.g. filenames are case sensitive
                    dic[KEY] = val

                KEYWORD = keyword.upper()
                if KEYWORD == "*NODE":
                    node_set_name = dic.get("NSET", None)
                    if node_set_name:
                        node_sets[node_set_name] = []
                    action = Action.GET_NODE_IDS_AND_COORDINATES

                elif KEYWORD == "*ELEMENT":
                    elem_type = dic["TYPE"]
                    elem_set_name = dic.get("ELSET", None)
                    if elem_set_name:
                        elem_sets[elem_set_name] = []
                    action = Action.GET_ELEM_IDS_AND_CONNECTIVITY

                elif KEYWORD == "*NSET":
                    node_set_name = dic["NSET"]
                    node_sets[node_set_name] = []
                    action = Action.GET_SET_IDS

                elif KEYWORD == "*ELSET":
                    elem_set_name = dic["ELSET"]
                    elem_sets[elem_set_name] = []
                    action = Action.GET_SET_IDS

                else:
                    # Unrecognized keyword
                    print(f"WARNING: Unrecognized keyword on line {line_number}: {keyword}".format())
                    action = None

    except:
        print(f"ERROR: Failed to parse line {line_number}.".format())
        print("\nContext:")
        print(context_around_line(line_number, lines, 5))
        print("Variables:")
        print(f"    line:            {line}".format())
        print(f"    action:          {action}".format())
        print(f"    line_is_comment: {line_is_comment}".format())
        print(f"    line_is_keyword: {line_is_keyword}".format())
        print(f"    data:             {data}".format())
        print(f"    data_is_complete: {data_is_complete}".format())
        raise

    # Return a dict of the mesh in similar order as INP file
    mesh = OrderedDict()
    mesh["node_ids"]    = node_ids
    mesh["node_coords"] = node_coords
    mesh["node_sets"]   = node_sets
    mesh["elem_ids"]    = elem_ids
    mesh["elem_types"]  = elem_types
    mesh["elem_conns"] = elem_conns
    mesh["elem_sets"]    = elem_sets
    return mesh


def convert_inp_to_mat(ifile, ofile):
    """Convert an INP file to a Matlab MAT file.

    To read a MAT file in Matlab do this:
    mesh = load(filename);
    """
    import scipy.io
    mesh = read_inp(ifile)
    scipy.io.savemat(ofile, mesh)


def convert_inp_to_json(ifile, ofile):
    """Convert an INP file to a JSON file.

    To read a JSON file in Matlab do this:
    mesh = jsondecode(fileread(filename));
    """
    import json
    mesh = read_inp(ifile)
    with open(ofile, 'w') as f:
        json.dump(mesh, f)


def context_around_line(line_number, lines, context):
    """Create a string of context around some lines.
    """
    msg = ""
    context = 5
    for i in range(max(1, line_number - context), min(line_number + context, len(lines)) + 1):
        if i == line_number:
            msg += "----> "
        else:
            msg += "      "
        msg += "{:>10}|  {}".format(i, lines[i - 1])
    return msg


def _parse_data(line):
    """Get Data from a line.

    The Data could be a list of node numbers for a node set,
    the element id and list of nodal connectivity, or
    any other list of numbers separated by commas
    that may be continued over more than one line
    if the line ends with a comma.

    The following rules apply to data lines:

    - A data line can include no more than 256 characters, including blanks.
      Trailing blanks are ignored.

    - All data items must be separated by commas (,).
      An empty data field is specified by omitting data between commas.
      Abaqus will use values of zero
      for any required numeric data that are omitted
      unless a default value is specified.

    - A line must contain only the number of items specified.

    - Empty data fields at the end of a line can be ignored.

    - Floating point numbers can occupy a maximum of 20 spaces
      including the sign, decimal point, and any exponential notation.

    - Floating point numbers can be given with or without an exponent.
      Any exponent, if input, must be preceded by
      E or D and an optional (–) or (+).
      The following line shows four acceptable ways
      of entering the same floating point number:

           -12.345    -1234.5E-2   -1234.5D-2   -1.2345E1

    - Integer data items can occupy a maximum of 9 digits.

    - Character strings can be up to 80 characters long and are not case sensitive.

    - Continuation lines are allowed in specific instances
      If allowed, such lines are indicated by a comma
      as the last character of the preceding line.
      A single data item cannot be entered over multiple lines.

    Returns
    -------
    data
        List of data.
    data_is_complete
        True if the list of data is complete.
    """
    # Remove line endings and whitespace
    line = line.strip()

    # Is there more data to follow this line (does line end with comma)?
    if line[-1] == ",":
        data_is_complete = False
    else:
        data_is_complete = True

    # Remove the trailing comma before splitting line
    line = line.rstrip(",")

    # Get the data separated by commas
    data = [i.strip() for i in line.split(",")]

    return data, data_is_complete


def _parse_node_id_and_coord(line):
    """Get the node ID and coordinates from a line
    """

    # Remove line endings and whitespace
    line = line.strip()

    # Get all the items separated by commas
    items = line.split(",")

    # Get the node ID
    node_id = int(items[0])

    # Get the node coordinates
    node_coord = [float(x) for x in items[1::]]

    return node_id, node_coord


if __name__ == "__main__":

    # Tests

    meshname = "mymesh"

    # Read INP file
    ifile = meshname + ".inp"
    mesh = read_inp(ifile)

    assert len(mesh['node_ids']) == 435
    assert len(mesh['node_coords']) == 435
    assert len(mesh['node_sets']['nall']) == 435
    assert len(mesh['node_sets']['zmin']) == 67
    assert len(mesh['node_sets']['zmax']) == 67
    assert len(mesh['elem_ids']) == 72
    assert len(mesh['elem_types']) == 72
    assert len(mesh['elem_conns']) == 72
    assert len(mesh['elem_sets']['eall']) == 72

    OUTPUT = False
    if OUTPUT:
        # Save as Matlab file
        mfile = meshname + ".mat"
        convert_inp_to_mat(ifile, mfile)

        # Save as JSON file
        jfile = meshname + ".json"
        convert_inp_to_json(ifile, jfile)

        # Save as NumPy file:
        import numpy as np
        def savenpy(fname, mesh):
            import numpy as np
            np.save(fname, mesh)
        savenpy("mymesh.npy", mesh)
        # and load
        def loadnpy(fname):
            return np.load("mymesh.npy").item()
        mesh2 = loadnpy("mymesh.npy")
        # and, finally, check that they are the same:
        assert mesh == mesh2

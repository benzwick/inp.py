# Inp.Py: import and export INP files in Python

Inp.Py is a Python module for working with INP files
commonly used with numerical simulation software
such as Abaqus and CalculiX.

Inp.Py (like the Python language itself)
is designed to be easy to read and extend.
As such, the INP file is read with only
very limited checking and only supports
parsing of a limited selection of keywords.

Only mesh entities (i.e. nodes, elements and sets) are imported,
everything else is silently ignored.
Surfaces are not yet supported.

## Usage

1. Read a mesh from an INP file using Python:

        import inp
        mesh = inp.read_inp("mymesh.inp")

2. Convert an INP file to an Octave or Matlab MAT file:

        inp.convert_inp_to_mat("mymesh.inp", "mymesh.mat")

    which can be read in Python using:

        import scipy.io
        mesh = scipy.io.loadmat("mymesh.mat");

    or in Julia using:

        import MAT
        mesh = MAT.matread("mymesh.mat");

    or in Octave or Matlab using:

        mesh = load('mymesh.mat');

3. Convert an INP file to a JSON file:

        inp.convert_inp_to_json("mymesh.inp", "mymesh.json")

    which can be read in Python using:

        import json
        with open("mymesh.json") as f:
            mesh = json.load(f)

    or in Julia using:

        import JSON
        mesh = JSON.parsefile("mymesh.json");

    or in Matlab using:

        mesh = jsondecode(fileread('mymesh.json'));

4. Save as NumPy file:

        def savenpy(fname, mesh):
            import numpy as np
            np.save(fname, mesh)

        savenpy("mymesh.npy", mesh)

    and load

        def loadnpy(fname):
            return np.load("mymesh.npy").item()

        mesh2 = loadnpy("mymesh.npy")

    and, finally, check that they are the same:

        mesh == mesh2

    which should be `True`.

## Copying

Inp.Py is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Inp.Py is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Inp.Py.  If not, see <https://www.gnu.org/licenses/>.
